#!/bin/sh

set -e

rsync -av --delete -e ssh --rsync-path=/usr/bin/rsync /home/rfo/Transition/ nastar:Sauvegardes/Transition/
