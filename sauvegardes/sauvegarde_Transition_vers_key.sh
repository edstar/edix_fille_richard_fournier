#!/bin/sh

set -e

if mount | grep -q "on /mnt/key "; then
  if [ -d /mnt/key/Transition ]; then
    rsync -av --delete /home/rfo/Transition/ /mnt/key/Transition/ && sync
  else
    >&2 printf "Erreur : /mnt/key/Transition n'existe pas\n"
    exit 1
  fi
else
  >&2 printf "Erreur : Aucun disque n'est monté sur /mnt/key\n"
  exit 1
fi
