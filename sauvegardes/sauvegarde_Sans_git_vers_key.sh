#!/bin/sh

set -e

if mount | grep -q "on /mnt/key "; then
  if [ -d /mnt/key/Sans_git ]; then
    rsync -av --delete /home/rfo/Sans_git/ /mnt/key/Sans_git/ && sync
  else
    >&2 printf "Erreur : /mnt/key/Sans_git n'existe pas\n"
    exit 1
  fi
else
  >&2 printf "Erreur : Aucun disque n'est monté sur /mnt/key\n"
  exit 1
fi
