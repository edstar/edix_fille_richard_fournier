#echo ">=dev-libs/xmlsec-1.3.2 nss" >> /tmp/package.use/xmlto
#echo ">=app-text/ghostscript-gpl-10.02.1 cups" >> /tmp/package.use/xmlto
#echo "net-print/cups usb zeroconf" >> /tmp/package.use/cups
#echo ">=net-dns/avahi-0.8-r7 python" >> /tmp/package.use/avahi
#echo "net-print/hplip doc scanner" >> /tmp/package.use/hplip
#echo ">=dev-python/pillow-10.1.0-r1 truetype tiff" >> /tmp/package.use/pillow
#echo ">=app-text/poppler-23.12.0 cairo" >> /tmp/package.use/poppler
#echo ">=media-libs/tiff-4.6.0 jpeg" >> /tmp/package.use/jpeg
#echo "media-gfx/sane-backends usb zeroconf" >> /tmp/package.use/sane-backends
#echo ">=virtual/libusb-1-r2 udev" >> /tmp/package.use/libusb
#echo ">=dev-libs/libusb-1.0.26 udev" >> /tmp/package.use/libusb
#echo ">=app-crypt/gcr-3.41.1-r2:0 gtk" >> /tmp/package.use/gcr
#echo ">=x11-libs/gdk-pixbuf-2.42.10-r1 jpeg" >> /tmp/package.use/gdk-pixbuf
echo "media-video/vlc dvd ffmpeg mpeg mad wxwindows aac dts a52 ogg flac theora oggvorbis matroska freetype bidi xv svga gnutls stream vlm httpd cdda vcd cdio live lua xml" >> /tmp/package.use/vlc
echo "media-libs/freetype harfbuzz" >> /tmp/package.use/freetype
#echo ">=dev-qt/qtbase-6.7.2-r5 wayland" >> /tmp/package.use/pcmanfm-qt
#echo ">=dev-libs/libpeas-1.36.0 gtk" >> /tmp/package.use/eog
#echo "USE=\"\${USE} dist-kernel\"" >> /tmp/make.conf
#echo "VIDEO_CARDS=\"nvidia\"" >> /tmp/make.conf
#echo "x11-drivers/nvidia-drivers NVIDIA-r2" >> /tmp/package.license/nvidia
#echo "sys-firmware/nvidia-firmware MIT NVIDIA-r2" >> /tmp/package.license/nvidia
#echo "x11-drivers/nvidia-drivers kernel-open" >> /tmp/package.use/nvidia
echo "dev-libs/libpeas gtk" >> /tmp/package.use/eog
echo "app-crypt/gcr gtk" >> /tmp/package.use/evince

