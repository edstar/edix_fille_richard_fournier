#!/bin/sh

set -e

printf "Mise à jour autre edix_fille\n"
if [ "${SYSTEME}" != "edix" ]; then
    >&2 printf "Erreur : vous ne travaillez pas sur le système edix\n"
    exit 1
fi
if [ "${USER}" != "root" ]; then
    >&2 printf "Erreur : L'utilisateur n'est pas root\n"
    exit 1
fi
if [ -n "${EDIX}" ]; then
    printf "Le répertoire edix est %s\n" "${EDIX}"
else
    >&2 printf "La variable d'environnement EDIX n'est pas définie ou est mal définie\n"
    exit 1
fi

if ! grep -q "wifi" "${EDIX}"/configuration.txt; then
  >&2 printf "Rajouter une ligne dans ${EDIX}/configuration.txt contenant soit wifi, soit nowifi\n"
  exit 1
fi
wifi="non"
if grep -q "wifi" "${EDIX}"/configuration.txt && ! grep -q "nowifi" "${EDIX}"/configuration.txt; then
  wifi="oui"
fi
if ! grep -q "reseauMaison" "${EDIX}"/configuration.txt; then
  >&2 printf "Rajouter une ligne dans ${EDIX}/configuration.txt contenant soit reseauMaison, soit noreseauMaison\n"
  exit 1
fi
reseauMaison="non"
if grep -q "reseauMaison" "${EDIX}"/configuration.txt && ! grep -q "noreseauMaison" "${EDIX}"/configuration.txt; then
  reseauMaison="oui"
fi
printf "Vous êtes sur le point de modifier les fichiers suivants :\n"
if [ "${wifi}" = "oui" ]; then
  printf " /etc/wpa_supplicant/wpa_supplicant.conf\n"
fi
printf " /etc/hostname\n"
if [ "${reseauMaison}" = "oui" ]; then
  printf " /etc/ssh/sshd_config.d/91_edix_fille_sshd.conf\n"
fi
printf " /etc/default/grub\n"
printf "Il s'agit d'une action de modification du système d'exploitation.\n"
printf "Souhaitez-vous continuer ? (O/N)\n"
read -r reponse
if [ "${reponse}" != "O" ]; then
    >&2 printf "Interruption\n"
    exit 1
fi
if [ "${wifi}" = "oui" ]; then
  "${EDIX}"/bin/copier_a_en_b.sh "${EDIX}"/edix_fille_prive/autre/etc/wpa_supplicant/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf
  rc-service wpa_supplicant restart
fi
"${EDIX}"/bin/copier_a_en_b.sh "${EDIX}"/edix_fille/autre/etc/hostname /etc/hostname
if [ "${reseauMaison}" = "oui" ]; then
  "${EDIX}"/bin/copier_a_en_b.sh "${EDIX}"/edix_fille/autre/etc/ssh/sshd_config.d/91_edix_fille_sshd.conf /etc/ssh/sshd_config.d/91_edix_fille_sshd.conf
  chmod 600 /etc/ssh/sshd_config.d/91_edix_fille_sshd.conf
fi
"${EDIX}"/bin/copier_a_en_b.sh "${EDIX}"/edix_fille/autre/etc/default/grub /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg
printf "Les anciennes versions sont actuellement archivée en\n"
if [ "${wifi}" = "oui" ]; then
  printf " /etc/wpa_supplicant/wpa_supplicant.conf.old\n"
fi
printf " /etc/hostname.old\n"
if [ "${reseauMaison}" = "oui" ]; then
  printf " /etc/ssh/sshd_config.d/91_edix_fille_sshd.conf.old\n"
fi
printf " /etc/default/grub.old\n"
printf "Souhaitez-vous les effacer ? (O/N)\n"
read -r reponse
if [ "${reponse}" = "O" ]; then
  if [ "${wifi}" = "oui" ]; then
    /bin/rm /etc/wpa_supplicant/wpa_supplicant.conf.old
  fi
  /bin/rm /etc/hostname.old
  if [ "${reseauMaison}" = "oui" ]; then
    /bin/rm /etc/ssh/sshd_config.d/91_edix_fille_sshd.conf.old
  fi
  /bin/rm /etc/default/grub.old
fi

printf "Mise à jour autre edix_fille : succès.\n"
