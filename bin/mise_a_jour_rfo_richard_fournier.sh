#!/bin/sh

set -e

printf "Mise à jour rfo\n"
if [ "${USER}" != "rfo" ]; then
    >&2 printf "Erreur : l'utilisateur n'est pas rfo\n"
    exit 1
fi
if [ -n "${EDIX}" ]; then
    printf "Le répertoire edix est %s\n" "${EDIX}"
else
    >&2 printf "La variable d'environnement EDIX n'est pas définie ou est mal définie\n"
    exit 1
fi

"${EDIX}"/bin/creer_un_lien_b_vers_une_cible_a.sh "${EDIX}/edix_fille/rfo/env/bashrc_rfo" "/home/rfo/.bashrc"
"${EDIX}"/bin/creer_un_lien_b_vers_une_cible_a.sh "${EDIX}/edix_fille/rfo/env/gitconfig_rfo" "/home/rfo/.gitconfig"
"${EDIX}"/bin/creer_un_lien_b_vers_une_cible_a.sh "${EDIX}/edix_fille/rfo/env/Xresources_rfo" "/home/rfo/.Xresources"
"${EDIX}"/bin/creer_un_lien_b_vers_une_cible_a.sh "${EDIX}/edix_fille_prive/rfo/env/ssh_rfo" "/home/rfo/.ssh"

printf "Mise à jour rfo : succès.\n"
